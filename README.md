1. level:
     - no error return type except that parsing did not work

2. level:
     - error return type that enables sophisticated error return types

3. level:
     - almost same as 2. level, but the parsers are generalized to allow to parse tokens consisting out of a character with an anodation. e.g. the number the character or line number and column number. This is mostly done by having a compare function as a parameter. This function knows how to compare an token to an Char for example. So the tokens get examined for attributes instead of equality to a Char.
