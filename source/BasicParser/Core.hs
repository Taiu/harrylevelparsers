module BasicParser.Core where

import Data.Either

import BasicParser.Import
import BasicParser.Missing

newtype Parser token falsity output
  = Parser
  { runParser :: [token] -> Either falsity ([token], output) }
  --             input             error    remnant  output

leftover :: ([token], output) -> [token]
leftover = fst
output :: ([token], output) -> output
output = snd

parse :: Parser token falsity output -> [token] -> Either falsity ([token], output)
parse parser string = (runParser parser) string

-- | assumes that parsing is possible and outputs result. errors on unsuccessful parsing.
parsed :: Show a => Parser token a output -> [token] -> ([token], output)
parsed parser input
  = either errorCase successCase
  $ parse parser input
  where
    successCase = id
    errorCase e = error $ "parsing was not successful:\n" ++ show e

parseOutput parser string
  = fmap output
  $ parse parser string
parsedOutput parser input
  = output
  $ parsed parser input

run :: [token] -> Parser token falsity output -> Either falsity ([token], output)
run = flip parse

assumeSuccessfulParsing input parserBoxed = result
  where
    parser = runParser parserBoxed
    digest = parser input
    result = case digest of
                  Left errors -> error $ show errors
                  Right output -> output

--TODO check all laws
instance Functor (Parser token falsity) where
  fmap f (Parser parser) = Parser $ fmap3 f parser

fmapOnParserError :: (f1 -> f2) -> (Parser t f1 o) -> (Parser t f2 o)
fmapOnParserError f (Parser parser) = Parser result
  where
    result input = fmapOnLeft f $ parser input

instance Applicative (Parser token falsity) where
  pure thing = Parser $ \queue -> Right (queue, thing)
  (Parser bigParser) <*> (Parser parser) = Parser result
    where
      result reststream = do--Either
        --TODO find out proper order of the next two lines:
        (reststream, tmpFunction) <- bigParser reststream
        (reststream, tmpOutput) <- parser reststream
        return $ (reststream, tmpFunction tmpOutput)

instance Monad (Parser token falsity) where
  (Parser parser) >>= f = Parser result
    where
      result string = do
        tmp <- parser string
        let tmpResult = output tmp
        let tmpStringRest = leftover tmp
        let newParser = f tmpResult
        runParser newParser tmpStringRest

firstRight :: (Either a1 b) -> (Either a2 b) -> (Either (a1, a2) b)
firstRight (Right x) _ = Right x
firstRight _ (Right y) = Right y
firstRight (Left x) (Left y) = Left (x,y)

--TODO implement elegantly
instance (Monoid falsity) => Alternative (Parser token falsity) where
  empty = Parser $ const $ Left mempty
  (Parser parser1) <|> (Parser parser2) = Parser result
    where
      result = mapLeft (uncurry (<>)) . first
      first input = firstRight (parser1 input) (parser2 input)

firstSuccessfulResult :: Semigroup a => [Either a b] -> Either a b
firstSuccessfulResult xs = result
  where
    result = maybeToRight allFalsities successPath
    successPath = listToMaybe $ rights xs
    allFalsities = (\[x,y] -> x <> y) $ lefts xs

instance (Monoid falsity) => MonadPlus (Parser token falsity) where
