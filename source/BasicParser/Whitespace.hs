module BasicParser.Whitespace where

import BasicParser.Import
import BasicParser.Core
import BasicParser.Errors
import BasicParser.ErrorMangling
import BasicParser.Missing
import BasicParser.LexerLike
import BasicParser.Basic
import BasicParser.SpecialToken

eatWhitespaceToken = eatOneOfValidTokens whitespace
eatLegacyWhitespaceToken
  = eatOneOfValidTokens $ whitespace ++ legacyWhitespace

eatSpaces = greedilyRepeatParser eatSpace
eatWhitespace = greedilyRepeatParser eatWhitespaceToken
eatLegacyWhitespace = greedilyRepeatParser eatLegacyWhitespaceToken
