module BasicParser.LexerLike where

import Data.Maybe

import BasicParser.Import
import BasicParser.Errors
import BasicParser.ErrorMangling
import BasicParser.Core
import BasicParser.IncompatibleErrorCompromise
import BasicParser.Basic
import BasicParser.Missing

eatNewLine = eatExpectedToken '\n'
eatNonNewLineToken = eatExpectedNotToken '\n'

--TODO rename to eat?
eatString :: Eq b => [b] -> Parser b (EatExpectedTokenError b) [b]
eatString = mapM eatExpectedToken

eatUntilEOL = fmap fst parsing
  where
    parsing = doTwoParser line eatNewLine
    line = greedilyRepeatParser eatNonNewLineToken

eatUntilEOF = greedilyRepeatParser eatOne

eatPOSIXLinesWithRemnants = doTwoParser (greedilyRepeatParser eatUntilEOL) (eatUntilEOF)

eatPOSIXLine lineParser
  = fmapOnParserError (either LineContentError UnexpectedContinuationOfLine)
  $ lineParser ⋖ eatString "\n"

--TODO add cannotFail
consumeUntilStringOrEndOfFile border
  = greedilyRepeatParser
  $ (negativeLookahead $ eatString border) ⋗ eatOne

consumeUntilCanParseOrEndOfFile parser
  = greedilyRepeatParser
  $ (negativeLookahead $ parser) ⋗ eatOne

firstMatch parser
  = wrapError ThereWasNoMatch
  $ consumeUntilCanParseOrEndOfFile parser *> parser

-- | never fails to parse. error type is still not () for it to be more compatible
greedyNonOverlapingMatches parser
  = greedilyRepeatParser
  $ firstMatch parser

greedyNonOverlapingMatchesHinted :: Parser t error o -> Parser t () [o]
greedyNonOverlapingMatchesHinted = greedyNonOverlapingMatches

-- | matches that are overlapping to the first match do not count. For example `eatString "xx"` matches only one time in `"xxx"`.
onlyOneMatch parser
  = fmapOnParserError enrichError
  $ fmap head
  $ eatAndCheckOutput isSingleton
  $ greedyNonOverlapingMatches parser
  where
    enrichError (UnexpectedOutput []) = NoMatchAtAll
    enrichError (UnexpectedOutput output) = MoreThanOneMatch output
    enrichError (UnexpectedParserError _)
      = undefined -- never happens because of greedyNonOverlapingMatches never failing

completeParse
  :: Parser t error o -> Parser t (CompleteParseError error [t]) o
completeParse parser
  = fmapOnParserError enrichError
  $ parser ⋖ checkForEOF undefined
  where
    enrichError (Left error)
      = CompleteParseInnerError error
    enrichError (Right (UnexpectedContinuationOfFile remnant))
      = UnexpectedContinuationOfFile2 remnant

--TODO refactor, remnant gets parsed twice
repeatParserForCompleteParse parser
  = fmapOnParserError enrichError
  $ completeParse
  $ cannotFail
  $ greedilyRepeatParser parser
  where
    enrichError (UnexpectedContinuationOfFile2 remnant)
      = id
      $ ParserCannotBeAppliedButStillTokensLeft
      $ fromLeft (error "this should not have happened")
      $ run remnant parser

streamEndsWith postfix
  = completeParse
  $ (consumeUntilCanParseOrEndOfFile $ completeParse $ eatString postfix)
  <* eatString postfix

eatTokenStream :: Eq o => [o] -> Parser o Void [o]
eatTokenStream tokens
  = greedilyRepeatParser
  $ eatOneOfValidTokens
  $ tokens

eatNonZeroTokenStream tokens
  = greedilyRepeatParserAtLeastOnce
  $ eatOneOfValidTokens
  $ tokens
