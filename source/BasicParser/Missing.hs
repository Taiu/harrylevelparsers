module BasicParser.Missing where

import BasicParser.Import

fmap1 :: Functor f
  => (a -> b) -> f a -> f b
fmap2 :: (Functor f1, Functor f2)
  => (a -> b) -> f1 (f2 a) -> f1 (f2 b)
fmap3 :: (Functor f1, Functor f2, Functor f3)
  => (a -> b) -> f1 (f2 (f3 a)) -> f1 (f2 (f3 b))

fmap1 = fmap
fmap2 = fmap . fmap
fmap3 = fmap . fmap . fmap
-- ...

app1 :: Applicative f => f (a -> b) -> f a -> f b

app1 = (<*>)
app2 meta thing = app1 (fmap1 app1 meta) thing
app3 meta thing = app2 (fmap2 app1 meta) thing
-- ...

takeWhileJust :: [Maybe a] -> [a]
takeWhileJust =
   foldr (\x acc -> maybe [] (:acc) x) []

ensure :: (a -> Bool) -> a -> Maybe a
ensure p v
  | p v       = Just v
  | otherwise = Nothing

switchOn :: Bool -> l -> r -> Either l r
switchOn switch l r = if switch then Right r else Left l

listToEither :: [r] -> Either l r
listToEither [] = Left undefined
listToEither (r:rs) = Right r

-- https://hackage.haskell.org/package/either-5.0.1.1/docs/src/Data.Either.Combinators.html#maybeToRight
maybeToRight :: b -> Maybe a -> Either b a
maybeToRight _ (Just x) = Right x
maybeToRight y Nothing  = Left y

-- https://hackage.haskell.org/package/either-5.0.1.1/docs/src/Data.Either.Combinators.html#swapEither
swapEither :: Either e a -> Either a e
swapEither = either Right Left
{-# INLINE swapEither #-}

--TODO rename to fmapBoth
--https://hackage.haskell.org/package/either-5.0.1.1/docs/src/Data.Either.Combinators.html#mapBoth
mapBoth :: (a -> c) -> (b -> d) -> Either a b -> Either c d
mapBoth f _ (Left x)  = Left (f x)
mapBoth _ f (Right x) = Right (f x)

mapLeft f = mapBoth f id

fmapOnLeft f = swapEither . (fmap f) . swapEither

mapBoth1 f = mapBoth f f

nubToOne :: Eq a => [a] -> [a]
nubToOne list = assert check result
  where
    check = case result of
                 [_] -> True
                 _ -> False
    result = nub list

nubToAtMostOne :: Eq a => [a] -> [a]
nubToAtMostOne list = assert check result
  where
    check = case result of
                 [] -> True
                 [_] -> True
                 _ -> False
    result = nub list

-- https://hackage.haskell.org/package/extra-1.7.3/docs/src/Data.List.Extra.html#nubOn
--TODO library has deprecated this function but did not offer a replacement
nubOn :: Eq b => (a -> b) -> [a] -> [a]
nubOn f = map snd . nubBy ((==) `on` fst) . map (\x -> let y = f x in y `seq` (y, x))

-- https://hackage.haskell.org/package/ghc-8.10.1/docs/src/Util.html#isSingleton
isSingleton :: [a] -> Bool
isSingleton [_] = True
isSingleton _   = False

mapFst f (x,y) = (f x, y)

--TODO implement efficiently
lastN n = reverse . (take n) . reverse

traceIdP s x = trace (s ++ show x ++ s) x

decide :: Bool -> a -> b -> Either a b
decide True _ b = Right b
decide False a _ = Left a

applyN f n = foldr (.) id (replicate n f)
