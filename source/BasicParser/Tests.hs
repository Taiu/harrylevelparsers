module BasicParser.Tests where

import BasicParser.Import
import BasicParser.Missing
import BasicParser.Core
import BasicParser.Errors
import BasicParser.ErrorMangling
import BasicParser.Basic
import BasicParser.LexerLike
import BasicParser.Particles

test = (,) "basic parser tests" $ and [test_A, test_B, test_C, test_D, test_E]

test_A = and [test1, test2, test3, test4, test5, test6, test7, test8, test9, test10]

test1 = (run "+123" (fmap (\x-> x:[x]) eatOne)) == Right ("123", "++")
test2 = (run "^" $ (Parser (\x -> Right (x, (\y -> y:[y])))  ) <*> eatOne) == Right ("","^^")
test3 = (==) 1 $ fromJust $ fromJust $ fromJust
  $ app3
    (Just $ Just $ Just (+1))
    (Just $ Just $ Just 0)
test4 = (==) (Right ("est","tt")) $ run "test" $ (<*>) (Parser (\x -> Right (x, (\x -> x:[x])))) eatOne
test5 = (==) (Right ("st",'e')) $ run "test" $ eatOne >>= (\o -> eatOne)
test6 = (==) (Right ("ftest",'q')) $ run "qftest" $ eatAndCheckOutput (== 'q') eatOne
test7 = (==) (Right ("45",('1',('2','3')))) $ run "12345" $ doTwoParser eatOne  $ doTwoParser eatOne eatOne
test8 = (==) "012345678910" $ take 12 $ output $ fromRight undefined $ run ( flip (++) "#" $ concat $ (map show [0..])) $ greedilyRepeatParser $ eatOneOfValidTokens "0123456789"
test9 = (==) (Right ("",("sdk","123"))) $ run "sdk\n123\n" $ doTwoParser eatUntilEOL eatUntilEOL
test10 = (==) (Right ("",(["1","2","3"],"4")) :: Either () (String, ([String], String))) $ run "1\n2\n3\n4" $ eatPOSIXLinesWithRemnants
--

test_B = and $ map (\n -> test_B_supposed n == test_B_case n ) [1..4]

fail1 = failParser "fail1"
fail2 = failParser "fail2"
succ1 = successParser $ (++) "success1"
succ2 = successParser $ "success2"

test_B_supposed 1 = (Left "fail1")
test_B_supposed 2 = (Left "fail1")
test_B_supposed 3 = (Left "fail2")
test_B_supposed 4 = (Right ("","success1success2"))
test_B_case 1 = run "" $ fail1 <*> fail2
test_B_case 2 = run "" $ fail1 <*> succ2
test_B_case 3 = run "" $ succ1 <*> fail2
test_B_case 4 = run "" $ succ1 <*> succ2


test_C = and [test_C_0, test_C_1, test_C_2, test_C_3, test_C_4, test_C_5, test_C_6, test_C_7, test_C_8, test_C_9, test_C_10, test_C_11, test_C_12, test_C_13, test_C_14, test_C_15, test_C_16]

test_C_0 = (==) (Right ("","EOF")) $ run "" $ checkForEOF "EOF"
test_C_1 = (==) ['0'] $ nub $ map (\(ExpectedToken_ButGot_ _ x) -> x) $ fromLeft undefined $ run "0" $ firstSuccessfulParser $ map (fmapOnParserError (\x -> [x]) . eatExpectedToken) "123"
test_C_2 = let extract (ExpectedToken_ButGotEndOfFile char) = char in (==) "123" $ fmap extract $ fromLeft undefined $ run "" $ firstSuccessfulParser $ map (fmapOnParserError (\x -> [x]) . eatExpectedToken) "123"
test_C_3 = (==) (Right ("",'2')) $ run "2" $ firstSuccessfulParser $ map (fmapOnParserError (\x -> [x]) . eatExpectedToken) "123"
test_C_4 = (==) (Left [] :: Either [()] (String, ())) $ run "123" $ firstSuccessfulParser []
test_C_5 = (==) (Left (ExpectedATokenFrom_ButGot_ "12345" 'q')) $ run "q" $ eatOneOfValidTokens "12345"
test_C_6 = (==) (Right ("",testdata) :: Either () (String, String)) $ run testdata $ greedilyRepeatParser (eatExpectedToken '#') where testdata = replicate 100000 '#'
test_C_7 = (==) (Right ("","") :: Either () (String, String)) $ run "" $ greedilyRepeatParser (eatExpectedToken '#')
--
equivS
  :: Either (EatExpectedTokenError Char) (String, String)
  -> Either (EatExpectedTokenError Char) (String, String)
  -> Bool
equivS = (==)
--
test_C_8 = equivS (Right ("","")) $ run "" $ greedilyRepeatParser (eatExpectedToken '#')
test_C_9 = equivS (Left (ExpectedToken_ButGot_ '1' 'x')) $ run "xyz" $ eatString "123"
test_C_10 = equivS (Right ("_qwe","123")) $ run "123_qwe" $ eatString "123"
test_C_11 = equivS (Right ("|","|_|_|_|_|_")) $ run "|_|_|_|_|_|" $ fmap concat $ greedilyRepeatParser $ eatString "|_"
test_C_12 = (==) (Right ("</>",())) $ run "</>" $ positiveLookahead $ eatString "</>"
test_C_13 = (==) (Left (PositiveLookaheadError (ExpectedToken_ButGot_ '/' '#'))) $ run "<#>" $ positiveLookahead $ eatString "</>"
test_C_14 = (==) (Left (PositiveLookaheadError (ExpectedToken_ButGotEndOfFile '/'))) $ run "<" $ positiveLookahead $ eatString "</>"
test_C_15 = (==) (Right ("#67890","12345") :: Either () (String, String)) $ run "12345#67890" $ consumeUntilStringOrEndOfFile "#"
test_C_16 = (==) (Right ("-",["-#-","-#-","-#-"]) :: Either () (String, [String])) $ run "+--#-------#-#----++-+--#--" $ greedyNonOverlapingMatches (eatString "-#-")

successCases = ["0", "1", "12", "123", "100^^", "0^^"]
failureCases = ["012^^", "012", "00", "x0", "x0x", "00x"]

test_D = and [test_D_0, test_D_1, test_D_2, test_D_3, test_D_4, test_D_5, test_D_6]

test_D_0 = (==) ["0","1","12","123","100","0"] $ map snd $ rights $ map (flip run $ eatDigitSequenceWithoutLeadingZeros) successCases

test_D_1 = (==) (Left (ExpectedNonDigitTokenButGot '1',ExpectedANonZeroDigitOutOf_ButGot_ "123456789" '0')) $ run "012^^" eatDigitSequenceWithoutLeadingZeros
test_D_2 = (==) (Left (ExpectedNonDigitTokenButGot '1',ExpectedANonZeroDigitOutOf_ButGot_ "123456789" '0')) $ run "012" eatDigitSequenceWithoutLeadingZeros
test_D_3 = (==) (Left (ExpectedNonDigitTokenButGot '0',ExpectedANonZeroDigitOutOf_ButGot_ "123456789" '0')) $ run "00" eatDigitSequenceWithoutLeadingZeros
test_D_4 = (==) (Left (ExpectedTokenZeroButGot_ 'x',ExpectedANonZeroDigitOutOf_ButGot_ "123456789" 'x')) $ run "x0" eatDigitSequenceWithoutLeadingZeros
test_D_5 = (==) (Left (ExpectedTokenZeroButGot_ 'x',ExpectedANonZeroDigitOutOf_ButGot_ "123456789" 'x')) $ run "x0x" eatDigitSequenceWithoutLeadingZeros
test_D_6 = (==) (Left (ExpectedNonDigitTokenButGot '0',ExpectedANonZeroDigitOutOf_ButGot_ "123456789" '0')) $ run "00x" eatDigitSequenceWithoutLeadingZeros

test_E = and [test_E_0, test_E_1, test_E_2, test_E_3, test_E_4, test_E_5, test_E_6, test_E_7, test_E_8, test_E_9, test_E_10, test_E_11]

test_E_0 = (==) (Right ("",("2000","01","01"))) $ run "2000-01-01" $ eatDate_iso8601
test_E_1 = (==) (Right ("",("0","00","00"))) $ run "0-00-00" $ eatDate_iso8601
test_E_2 = (==) (Right ("",("999999","00","00"))) $ run "999999-00-00" $ eatDate_iso8601

test_E_3 = (==) (Left (ExpectedYearBut (ExpectedTokenZeroButGotEndOfFile,ExpectedANonZeroDigitOutOf_ButGotEndOfFile "123456789"))) $ run "" $ eatDate_iso8601
test_E_4 = (==) (Left (ExpectedYearBut (ExpectedTokenZeroButGotEndOfFile,ExpectedANonZeroDigitOutOf_ButGotEndOfFile "123456789"))) $ run "" $ eatDate_iso8601
test_E_5 = (==) (Left (ExpectedYearBut (ExpectedNonDigitTokenButGot '0',ExpectedANonZeroDigitOutOf_ButGot_ "123456789" '0'))) $ run "00-0-0" $ eatDate_iso8601
test_E_6 = (==) (Left (ExpectedYearBut (ExpectedNonDigitTokenButGot '1',ExpectedANonZeroDigitOutOf_ButGot_ "123456789" '0'))) $ run "01-10-10" $ eatDate_iso8601
test_E_7 = (==) (Left (ExpectedYearBut (ExpectedNonDigitTokenButGot '0',ExpectedANonZeroDigitOutOf_ButGot_ "123456789" '0'))) $ run "0000-0-0" $ eatDate_iso8601
test_E_8 = (==) (Left (ExpectedDayBut (ExpectedATokenFrom_ButGotEndOfFile "0123456789"))) $ run "10000-01-0" $ eatDate_iso8601
test_E_9 = (==) (Left (ExpectedDayBut (ExpectedATokenFrom_ButGot_ "0123456789" 'p'))) $ run "10000-01-0p" $ eatDate_iso8601
test_E_10 = (==) (Left (ExpectedHyphenAsSeparatorBut (ExpectedToken_ButGot_ '-' '_'))) $ run "10000_01_0p" $ eatDate_iso8601
test_E_11 = (==) (Left (ExpectedHyphenAsSeparatorBut (ExpectedToken_ButGotEndOfFile '-'))) $ run "20201010" $ eatDate_iso8601

