module BasicParser.Particles where

import Data.Maybe

import BasicParser.Import

import BasicParser.Errors
import BasicParser.ErrorMangling
import BasicParser.Core
import BasicParser.IncompatibleErrorCompromise
import BasicParser.Basic
import BasicParser.LexerLike
import BasicParser.SpecialToken

-- eatZero :: Parser Char (ParseError Char) String
eatOneZero = eatString "0"

eatSingleNonZeroDigit :: Parser Char (EatOneOfValidTokensError String Char) String
eatSingleNonZeroDigit = fmap (\x -> [x]) eatNonZeroDigit
eatSingleDigit = fmap (\x -> [x]) eatDigit

-- eatDigitSequence :: Parser Char f2 String
eatDigitSequence = greedilyRepeatParser eatDigit

onlyZero
  = fmapOnParserError enrichError
  $ eatOneZero ⋖ (negativeLookahead eatDigit)
  where
    enrichError (Left (ExpectedToken_ButGot_ token output))
      = assert (head zeroDigit == token)
      $ ExpectedTokenZeroButGot_ output
    enrichError (Left (ExpectedToken_ButGotEndOfFile token))
      = assert (head zeroDigit == token)
      $ ExpectedTokenZeroButGotEndOfFile
    enrichError (Right (NegativeLookaheadError (remnant, token)))
      = assert (token `elem` digits)
      $ ExpectedNonDigitTokenButGot token

nonZeroLeadingDigits
  = fmapOnParserError enrichError
  $ eatSingleNonZeroDigit ∺ eatDigitSequence
  where
    enrichError (ExpectedATokenFrom_ButGotEndOfFile possibleTokens)
      = ExpectedANonZeroDigitOutOf_ButGotEndOfFile possibleTokens
    enrichError (ExpectedATokenFrom_ButGot_ possibleTokens actualToken)
     = ExpectedANonZeroDigitOutOf_ButGot_ possibleTokens actualToken
    enrichError (ListOfValidTokensIsEmpty) = undefined

eatDigitSequenceWithoutLeadingZeros
  :: Parser
     Char
     (OnlyZeroError Char, NonZeroLeadingDigitsError String Char)
     [Char]
eatDigitSequenceWithoutLeadingZeros
  = onlyZero <¦> nonZeroLeadingDigits
eatTwoDigits = eatSingleDigit ∺ eatSingleDigit

eatYear
  = wrapError ExpectedYearBut
  $ eatDigitSequenceWithoutLeadingZeros

eatMonth
  = wrapError ExpectedMonthBut
  $ eatTwoDigits

eatDay
  = wrapError ExpectedDayBut
  $ eatTwoDigits

eatSeparator
  = wrapError ExpectedHyphenAsSeparatorBut
  $ eatMinus

eatDate_iso8601 = do
  year <- eatYear
  eatSeparator
  month <- eatMonth
  eatSeparator
  day <- eatDay
  return (year, month, day)

showDate (yyyy,mm,dd)
  = assert check $ yyyy ++ "-" ++ mm ++ "-" ++ dd
  where
    check = length mm == 2 && length dd == 2
