module BasicParser.IncompatibleErrorCompromise where

-- TODO rename to IncompatibleTypesCompromise

import BasicParser.Import
import BasicParser.Missing

import BasicParser.Core
import BasicParser.ErrorMangling
import BasicParser.Basic

infixl 3 <¦>
(<¦>) :: Parser t f1 o -> Parser t f2 o -> Parser t (f1, f2) o
(Parser parser1) <¦> (Parser parser2) = Parser result
  where
    result input = firstRight (parser1 input) (parser2 input)

-- like `*>` and `<*` but allowing two different falsities
infixl 4 ⋗, ⋖
(⋗)
  :: (Parser t f1 o1)
  -> (Parser t f2 o2)
  -> (Parser t (Either f1 f2) o2)
x ⋗ y = (fmapOnParserError Left x) *> (fmapOnParserError Right y)
(⋖)
  :: (Parser t f1 o1)
  -> (Parser t f2 o2)
  -> (Parser t (Either f1 f2) o1)
x ⋖ y = (fmapOnParserError Left x) <* (fmapOnParserError Right y)


--TODO How to choose associativity? Make possible: `a </$\> b </$\> c </\> d`
infixr 3 </\>, </$\>
(</\>) :: Parser t f1 o -> Parser t f1 o -> Parser t [f1] o
(</\>) = (<|>) `on` (fmapOnParserError (\x -> [x]))

x </$\> y = (fmapOnParserError (\q -> [q]) x) <|> (y)

firstApplicableParser
  :: [Parser t f o] -> Parser t (FirstApplicableParserError f) o
firstApplicableParser parsers
  = fmapOnParserError enrichError
  $ firstSuccessfulParser
  $ fmap (fmapOnParserError return) parsers
  where
    -- TODO maybe do not enrich the error
    enrichError [] = CannotFindAnApplicableParserFromZeroParsers
    enrichError es = NonOfTheAllowedParsersWorked es

data FirstApplicableParserError parser
  = CannotFindAnApplicableParserFromZeroParsers
  | NonOfTheAllowedParsersWorked [parser]
  deriving (Show)

wrap :: (o1 -> o2) -> (f1 -> f2) -> (Parser t f1 o1) -> (Parser t f2 o2)
wrap wrapOutput wrapError = (fmapOnParserError wrapError) . (fmap wrapOutput)
