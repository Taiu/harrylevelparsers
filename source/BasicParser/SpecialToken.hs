module BasicParser.SpecialToken where

import BasicParser.Import
import BasicParser.Core
import BasicParser.Errors
import BasicParser.ErrorMangling
import BasicParser.Missing
import BasicParser.LexerLike
import BasicParser.Basic

--TODO everything to just Strings

zeroDigit = "0"
digitsWithoutZero = "123456789"
digits = zeroDigit ++ digitsWithoutZero

englishLettersLowerCase = "abcdefghijklmnopqrstuvwxyz"
englishLettersUpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
englishLetters = englishLettersLowerCase ++ englishLettersUpperCase

eatDigit = eatOneOfValidTokens $ digits
eatNonZeroDigit = eatOneOfValidTokens $ digitsWithoutZero

eatSpace = eatString " "
eatHyphen = eatString "‐"
eatMinus = eatString "-"
eatDot = eatString "."
eatBackslash = eatString "\\"

whitespace = "\n "
legacyWhitespace = "\t\r"
space = " "
newLine = "\n"
