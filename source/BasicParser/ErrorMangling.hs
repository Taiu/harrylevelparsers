module BasicParser.ErrorMangling where

import BasicParser.Errors
import BasicParser.Missing
import BasicParser.Core

import BasicParser.Import

wrapError = fmapOnParserError

--TODO Probable should be removed after () to Void transition
discardError = wrapError $ const ()

wrapEatExpectedTokenError mismatchCase eofCase
  = result
  where
    result (ExpectedToken_ButGot_ _expectedToken actualToken)
      = mismatchCase actualToken
    result (ExpectedToken_ButGotEndOfFile _expectedToken)
      = eofCase

wrapEatExpectedTokenErrorCase x y = fmapOnParserError $ wrapEatExpectedTokenError x y

-- | since the parser never fails its error type can be arbitrary. For convenience there is an unused argument that is used to determine the error type
neverFails_ :: f -> Parser t Void o -> Parser t f o
neverFails_ _ = fmapOnParserError absurd

neverFails = neverFails_ undefined

cannotFail :: Parser t b o -> Parser t Void o
cannotFail
  = fmapOnParserError
  $ const $ error "These aren’t the errors you’re looking for."
