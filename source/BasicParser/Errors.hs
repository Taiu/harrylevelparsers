-- EOF = end of file
-- TODO should these errors be at the same place where the accompaning function ist?

module BasicParser.Errors
  (
    module BasicParser.Errors
  ) where

data ExpectedATokenButGotEndOfFile
   = ExpectedATokenButGotEndOfFile
      deriving (Eq, Show)

data UnexpectedContinuationOfFile remnant
   = UnexpectedContinuationOfFile remnant
      deriving (Eq, Show)

data ExpectedTokenButGotEndOfFile token
   = ExpectedTokenButGotEndOfFile token
      deriving (Eq, Show)

data EatExpectedNotTokenError token
   = EatExpectedNotTokenError token
      deriving (Eq, Show)

data EatAndCheckOutputError error output
  = UnexpectedParserError error
  | UnexpectedOutput output
    deriving (Eq, Show)

data EatExpectedTokenError token
  = ExpectedToken_ButGot_ token token
  | ExpectedToken_ButGotEndOfFile token
    deriving (Eq, Show)

type ParseError value = EatAndCheckOutputError (ExpectedTokenButGotEndOfFile value) value

data EatOneOfValidTokensError possibleTokens actualToken
  = ExpectedATokenFrom_ButGotEndOfFile possibleTokens
  | ExpectedATokenFrom_ButGot_ possibleTokens actualToken
  | ListOfValidTokensIsEmpty
    deriving (Eq, Show)

data PositiveLookaheadError error
   = PositiveLookaheadError error
      deriving (Eq, Show)

data NegativeLookaheadError parsedValue
   = NegativeLookaheadError parsedValue
      deriving (Eq, Show)

data CompleteParseError error remnant
   = CompleteParseInnerError error
   | UnexpectedContinuationOfFile2 remnant
      deriving (Eq, Show)

data RepeatParserForCompleteParseError error
   = ParserCannotBeAppliedButStillTokensLeft error
      deriving (Eq, Show)

data OnlyZeroError token
   = ExpectedTokenZeroButGot_ token
   | ExpectedTokenZeroButGotEndOfFile
   | ExpectedNonDigitTokenButGot token
      deriving (Eq, Show)

--TODO parameter for ExpectedANonZeroDigitOutOf_ButGotEndOfFile is not neccessary?
data NonZeroLeadingDigitsError possibleTokens actualToken
   = ExpectedANonZeroDigitOutOf_ButGotEndOfFile possibleTokens
   | ExpectedANonZeroDigitOutOf_ButGot_ possibleTokens actualToken
    deriving (Eq, Show)

data EatDate_iso8601Error yearError monthError dayError token
   = ExpectedYearBut yearError
   | ExpectedMonthBut monthError
   | ExpectedDayBut dayError
   | ExpectedHyphenAsSeparatorBut token
    deriving (Eq, Show)

--TODO two different names or same name? What is better?
data FirstMatchError error = ThereWasNoMatch error
    deriving (Eq, Show)

data OnlyOneMatchError output
  = UnexpectedParserError2
  | NoMatchAtAll
  | MoreThanOneMatch output
    deriving (Eq, Show)

data QuickError = QuickError String

--TODO name!
data EatCurlyBracesEmbracingError token error
  = ExpectedOpeningCurlyBraceButGot_ token
  | ExpectedOpeningCurlyBraceButGotEOF
  | ExpectedClosingCurlyBraceButGot_ token
  | ExpectedClosingCurlyBraceButGotEOF
  | ExpectedOpeningBracketButGot_ token
  | ExpectedOpeningBracketButGotEOF
  | ExpectedClosingBracketButGot_ token
  | ExpectedClosingBracketButGotEOF
  | UnexpectedCurlyBracesEmbracingInnerParserError error
  | UnexpectedBracketEmbracingInnerParserError error
    deriving (Eq, Show)

data LatexCommandError
  commandNameParserError
  curlyBracesEmbracingError
  parameterParserError
  token
  = ExpectedCommandBeginBackslashButGot_ token
  | ExpectedCommandBeginBackslashButGotEOF
  | UnexpectedCommandNameParserError commandNameParserError
  | ParameterEmbracingError curlyBracesEmbracingError
  | UnexpectedParameterContentParserError parameterParserError
    deriving (Eq, Show)

data BracketError left right inner token error
  = ExpectedOpeningBracket_ButGot_ left token
  | ExpectedOpeningBracket_ButGotEOF left
  | ExpectedClosingBracket_ButGot_ right token
  | ExpectedClosingBracket_ButGotEOF right
  | UnexpectedInnerParserError error
    deriving (Eq, Show)

data POSIXlineError a b
  = LineContentError a
  | UnexpectedContinuationOfLine b
    deriving (Eq, Show)
