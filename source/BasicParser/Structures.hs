module BasicParser.Structures where

import BasicParser.Errors
import BasicParser.ErrorMangling
import BasicParser.LexerLike

eatOpeningBracket leftString
  = wrapError
      ( wrapEatExpectedTokenError
        (ExpectedOpeningBracket_ButGot_ leftString)
        (ExpectedOpeningBracket_ButGotEOF leftString)
      )
  $ eatString leftString

eatClosingBracket rightString
  = wrapError
      ( wrapEatExpectedTokenError
        (ExpectedClosingBracket_ButGot_ rightString)
        (ExpectedClosingBracket_ButGotEOF rightString)
      )
  $ eatString rightString

-- TODO maybe leftString and rightString should be generalized to a parser?
eatBracketEmbracing leftString rightString innerParser = do
  eatOpeningBracket leftString
  inner <- wrapError UnexpectedInnerParserError innerParser
  eatClosingBracket rightString
  return inner
