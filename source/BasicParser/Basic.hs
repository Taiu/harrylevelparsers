module BasicParser.Basic where

import BasicParser.Import
import BasicParser.Core
import BasicParser.Errors
import BasicParser.ErrorMangling
import BasicParser.Missing


failParser :: falsity -> Parser token falsity output
failParser massage = Parser $ \_ -> Left massage

successParser :: output -> Parser token falsity output
successParser message = return message

maybeFailParser (Left error) = failParser error
maybeFailParser (Right success) = successParser success

-- | this parser never fails
-- TODO rename, make all more specific
discardAllParser :: Parser t f ()
discardAllParser = Parser $ const $ Right ([], ())

discardRemnent = (<* discardAllParser)

checkForEOF outputIfSuccessful = Parser $ parser
  where
    parser [] = Right ([], outputIfSuccessful)
    parser xs = Left $ UnexpectedContinuationOfFile xs

eatOne = Parser $ parser
  where
    parser (c:cs) = Right (cs, c)
    parser [] = Left $ ExpectedATokenButGotEndOfFile

doTwoParser :: (Parser t f o) -> (Parser t f p) -> (Parser t f (o,p))
doTwoParser = liftA2 (,)

-- do the parser with output. Addidionally fail to Left if predicate is false
eatAndCheckOutput :: (output -> Bool) -> Parser token falsity output -> Parser token (EatAndCheckOutputError falsity output) output
eatAndCheckOutput check (Parser parser) = Parser result
  where
    --TODO Is there a better approach to wrapError?
    wrapError = fmapOnLeft UnexpectedParserError
    result input = do--Either
      (restStream, value) <- wrapError $ parser input
      switchOn (check value) (UnexpectedOutput value) (restStream, value)

eatExpectedToken :: Eq o => o -> Parser o (EatExpectedTokenError o) o
eatExpectedToken token
  = fmapOnParserError (enrichError token)
  $ eatAndCheckOutput (== token) eatOne
  where
    enrichError
      :: a
      -> EatAndCheckOutputError ExpectedATokenButGotEndOfFile a
      -> EatExpectedTokenError a
    enrichError token (UnexpectedParserError ExpectedATokenButGotEndOfFile)
      = (ExpectedToken_ButGotEndOfFile token)
    enrichError token (UnexpectedOutput output)
      = (ExpectedToken_ButGot_ token output)

-- TODO generalize from token to String
eatExpectedNotToken token = eatAndCheckOutput (/= token) eatOne

firstSuccessfulParser :: Monoid f => [Parser t f o] -> Parser t f o
firstSuccessfulParser = foldr (<|>) $ failParser mempty

summarizeErrors :: (EatExpectedTokenError a) -> (EatExpectedTokenError a) -> Bool
summarizeErrors
  (ExpectedToken_ButGot_ _ _)
  (ExpectedToken_ButGot_ _ _)
    = True
summarizeErrors
  (ExpectedToken_ButGotEndOfFile _)
  (ExpectedToken_ButGotEndOfFile _)
    = True
summarizeErrors x y = undefined

eatOneOfValidTokens possibleTokens
  = assert check
  $ fmapOnParserError errorTranslation
  $ fmapOnParserError (nubBy summarizeErrors) --TODO some kind of fold
  $ firstSuccessfulParser parsers
  where
    check = nub possibleTokens == possibleTokens
    parsers = fmap parser possibleTokens
    parser = fmapOnParserError (\x -> [x]) . eatExpectedToken

    errorTranslation []
      = ListOfValidTokensIsEmpty
    errorTranslation [ExpectedToken_ButGot_ token output]
      = ExpectedATokenFrom_ButGot_ possibleTokens output
    errorTranslation [ExpectedToken_ButGotEndOfFile _]
      = ExpectedATokenFrom_ButGotEndOfFile possibleTokens

parseAllInSequence :: [Parser t f o] -> Parser t f [o]
parseAllInSequence = sequenceA

addParser :: [Parser t f [o]] -> Parser t f [o]
addParser = fmap concat . parseAllInSequence

infixr 5 ∺
(∺) :: Parser t f [o] -> Parser t f [o] -> Parser t f [o]
x ∺ y = addParser [x,y]

-- parser cannot fail. should `f2` be `()`? Answer: Void is probably the correct thing.
-- the provided parser is supposed to never successfully eat the empty word. otherwise a loop will appear
greedilyRepeatParser :: Parser t f o -> Parser t f2 [o]
greedilyRepeatParser (Parser parser) = Parser $ result
  where
    result input = help $ parser input
      where
        help (Left _) = Right (input, [])
        help (Right (s, o)) = Right $ combine (undefined, o) (recursion s)
  --recursion :: [t] -> ([t], [o])
    recursion s
      = fromRight undefined
      $ run s (greedilyRepeatParser (Parser parser))
  --combine :: (x,y) -> (x,[y]) -> (x, [y])
    combine (s,o) lazy_recursion = (left, o : right)
      where
        -- patternmatching in function paramter instead of the following would be a bug:
        left = fst lazy_recursion
        right = snd lazy_recursion

greedilyRepeatParserAtLeastOnce parser
  = addParser
  [ fmap return parser
  , greedilyRepeatParser parser
  ]

--TODO is supposed to realize that the parser parses the empty string. Therefor this is implemented incorrectly. But how to check, that nothing got parsed?
greedilyRepeatProductiveParser :: Parser t o [a] -> Parser t f2 [[a]]
greedilyRepeatProductiveParser = greedilyRepeatParser . checkForNonEmptyOutput

checkForNonEmptyOutput
  :: Foldable t =>
     Parser token falsity (t a)
     -> Parser token (EatAndCheckOutputError falsity (t a)) (t a)
checkForNonEmptyOutput = eatAndCheckOutput (not . null)

greedilyRepeatProductiveParserVoid :: Parser t f [a] -> Parser t Void [[a]]
greedilyRepeatProductiveParserVoid = greedilyRepeatProductiveParser

positiveLookahead :: Parser t e o -> Parser t (PositiveLookaheadError e) ()
positiveLookahead (Parser parser)
  = fmapOnParserError PositiveLookaheadError
  $ Parser result
  where
    result input = do--Either
        _ <- parser input
        return (input, ())

negativeLookahead
  :: Parser t f o
  -> Parser t (NegativeLookaheadError ([t], o)) ()
negativeLookahead (Parser parser)
  = fmapOnParserError NegativeLookaheadError
  $ Parser result
  where
    result input = case parser input of
                        (Left error) -> return (input, ())
                        (Right success) -> Left success
