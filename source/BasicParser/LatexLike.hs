module BasicParser.LatexLike where

import Data.Maybe

import BasicParser.Import
import BasicParser.Errors
import BasicParser.ErrorMangling
import BasicParser.Core
import BasicParser.IncompatibleErrorCompromise
import BasicParser.Basic
import BasicParser.LexerLike
import BasicParser.SpecialToken
import BasicParser.Particles
import BasicParser.Whitespace
import BasicParser.Structures

data LatexCommand commandName commandParameter
  = LatexCommand
  { name :: commandName
  , parameter :: commandParameter
  }
  deriving (Eq, Show)

--TODO refactor
eatOpeningBracket''
  = fmapOnParserError (wrapEatExpectedTokenError
      ExpectedOpeningBracketButGot_
      ExpectedOpeningBracketButGotEOF)
  $ eatString "["

--TODO refactor
eatClosingBracket''
  = fmapOnParserError (wrapEatExpectedTokenError
      ExpectedClosingBracketButGot_
      ExpectedClosingBracketButGotEOF)
  $ eatString "]"

eatOpeningCurlyBrace
  = fmapOnParserError (wrapEatExpectedTokenError
      ExpectedOpeningCurlyBraceButGot_
      ExpectedOpeningCurlyBraceButGotEOF)
  $ eatString "{"

eatClosingCurlyBrace
  = fmapOnParserError (wrapEatExpectedTokenError
      ExpectedClosingCurlyBraceButGot_
      ExpectedClosingCurlyBraceButGotEOF)
  $ eatString "}"

--TODO refactor:
eatBracketEmbracing_better = eatBracketEmbracing "[" "]"
eatBracketEmbracing'' innerParser = do
  eatOpeningBracket''
  inner <- wrapError UnexpectedBracketEmbracingInnerParserError innerParser
  eatClosingBracket''
  return inner

--TODO refactor:
eatCurlyBracesEmbracing innerParser = do
  eatOpeningCurlyBrace
  inner <- wrapError UnexpectedCurlyBracesEmbracingInnerParserError innerParser
  eatClosingCurlyBrace
  return inner

eatLatexCommandBeginningSymbol
  = wrapEatExpectedTokenErrorCase
    ExpectedCommandBeginBackslashButGot_ ExpectedCommandBeginBackslashButGotEOF
  $ eatBackslash

eatLatexParameter parameterContentParser = do
  parameterContent <- eatCurlyBracesEmbracing parameterContentParser
  return parameterContent

eatSimpleLatexCommand commandNameParser parameterContentParser = do
  eatLatexCommandBeginningSymbol
  commandName
    <- wrapError UnexpectedCommandNameParserError commandNameParser
  ignoreFilling
  parameter
    <- fmapOnParserError enrichError $ eatCurlyBracesEmbracing parameterContentParser
  return $ LatexCommand {name=commandName, parameter=parameter}
  where

enrichError (UnexpectedCurlyBracesEmbracingInnerParserError error)
  = UnexpectedParameterContentParserError error
enrichError (UnexpectedBracketEmbracingInnerParserError error)
  = UnexpectedParameterContentParserError error
enrichError error
  = ParameterEmbracingError error

eatLatexCommand commandNameParser bracketParser curlyBraceParser = do
  eatLatexCommandBeginningSymbol
  commandName
    <- wrapError UnexpectedCommandNameParserError commandNameParser
  ignoreFilling
  bracketParameters
    <- fmapOnParserError enrichError $ parseAllInSequence $ map (<* ignoreFilling) $ map eatBracketEmbracing'' $ bracketParser
  ignoreFilling
  curlyBraceParameters
    <- fmapOnParserError enrichError $ parseAllInSequence $ map (<* ignoreFilling) $ map eatCurlyBracesEmbracing $ curlyBraceParser
  return $ (commandName, bracketParameters, curlyBraceParameters)

eatAllLatexCommandBracketParameterContent
  = consumeUntilStringOrEndOfFile "]"

eatAllLatexCommandCurlyBraceParameterContent
  = fmap concat
  $ greedilyRepeatParser
  $ eatCurlyBracesEmbracing recursion
  <¦> eatAtLeastOneTokenUntileAnyCurlyBrace
  where
    recursion
      = fmap
        (\x -> "{"++ x ++"}")
        eatAllLatexCommandCurlyBraceParameterContent

eatAtLeastOneTokenUntileAnyCurlyBrace
  = fmapOnParserError enrichError
  $ checkForNonEmptyOutput eatUntileAnyCurlyBrace
  where
    enrichError (UnexpectedOutput [])
      = QuickError "parser needs to eat at least one token"
    enrichError (UnexpectedParserError _)
      = undefined

eatUntileAnyCurlyBrace
  = consumeUntilCanParseOrEndOfFile (eatOneOfValidTokens "{}")

ignoreFilling
  = fmap concat
  $ greedilyRepeatParser
  $ (checkForNonEmptyOutput eatLegacyWhitespace) <¦> eatLatexComment

eatLatexComment = do
  eatString "%"
  comment <- consumeUntilStringOrEndOfFile "\n"
  eatString "\n"
  return comment
