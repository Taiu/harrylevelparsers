module CleanParser.Tree where

import Numeric.Natural

class Indentation a where
  getIndentation :: a -> Natural

