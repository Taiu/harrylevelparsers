module CleanParser.Tests where

import Data.Either.Combinators

import CleanParser.API


test = (,) "list parser test" $ and [True
  , isLeft $ listParserTest ""
  , isRight $ listParserTest "1"
  , isRight $ listParserTest "1,2"
  , isRight $ listParserTest "1,2,3"
  , isRight $ listParserTest "1,2,3,4,5,6,7,8,9"
  , isRight $ listParserTest "1,,,,"
  , noTrailingCommaTest
  , isLeft $ listParserTest ","
  ]

listParserTest = runParser parser
  where
    parser = delimitedGreedilyRepeatParserAtLeastOnce digit delimiter
    delimiter = eatExpectedCharacter ','
    digit = eatOneCharOfString "0123456789"

noTrailingCommaTest
  = (\x -> case x of {[] -> True; _ -> False})
  $ (\(GreedilyRepeatOutput x)->x)
  $ (\(DelimitedGreedilyRepeatParserAtLeastOnceResult _ y _) -> y)
  $ getOutput
  $ fromRight'
  $ listParserTest
  $ "1,,,,"
