import System.Exit

import AbecedarianParser.Basic
import AbecedarianParser.Core
import AbecedarianParser.LexerLike
import AbecedarianParser.Missing
import AbecedarianParser.Tests

import BasicParser.Basic
import BasicParser.Core
import BasicParser.ErrorMangling
import BasicParser.Errors
import BasicParser.Import
import BasicParser.IncompatibleErrorCompromise
import BasicParser.LatexLike
import BasicParser.LexerLike
import BasicParser.Missing
import BasicParser.Particles
import BasicParser.SpecialToken
import BasicParser.Structures
import BasicParser.Tests
import BasicParser.Whitespace

import CleanParser.ActualUsingTheParser
import CleanParser.Basic
import CleanParser.Core
import CleanParser.Error
import CleanParser.LexerLike
import CleanParser.LowLevelBuildingBlocks
import CleanParser.UnicodeAware
import CleanParser.RenameInterfaceBehaviour
import CleanParser.Tests

main :: IO ()
main = do
  print "it compiles"

  let testsuite = tail $ [ undefined

        , AbecedarianParser.Tests.test
        , BasicParser.Tests.test
        , CleanParser.Tests.test

        , CleanParser.LowLevelBuildingBlocks.test_greedilyRepeatParser_works_lazy
        , CleanParser.LowLevelBuildingBlocks.test_optionalParse
        , CleanParser.LexerLike.test_completeParse
        , CleanParser.UnicodeAware.test_eatLine
        ]

  mapM_ runTestCase testsuite
  return ()

runTestCase (name, testresult)
  = do
    case name of
         [] -> error "empty name of test"
         _ -> return ()
    putStr name
    putStrLn ": ..."
    if testresult
     then putStrLn $ "  TEST SUCCESS"
     else do
       putStrLn $ "  TEST FAILURE"
       exitWith (ExitFailure 1)
