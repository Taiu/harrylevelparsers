module AbecedarianParser.Core where

import AbecedarianParser.Import

fmap1 :: Functor f
  => (a -> b) -> f a -> f b
fmap2 :: (Functor f1, Functor f2)
  => (a -> b) -> f1 (f2 a) -> f1 (f2 b)
fmap3 :: (Functor f1, Functor f2, Functor f3)
  => (a -> b) -> f1 (f2 (f3 a)) -> f1 (f2 (f3 b))

fmap1 = fmap
fmap2 = fmap . fmap
fmap3 = fmap . fmap . fmap
-- ...

app1 :: Applicative f => f (a -> b) -> f a -> f b

app1 = (<*>)
app2 meta thing = app1 (fmap1 app1 meta) thing
app3 meta thing = app2 (fmap2 app1 meta) thing
-- ...

newtype Parser token output
  = Parser
  { runParser :: [token] -> Maybe ([token], output) }

leftover = fst
output = snd

--TODO check all laws
instance Functor (Parser token) where
  fmap f (Parser parser) = Parser $ fmap3 f parser

instance Applicative (Parser token) where
  pure thing = Parser $ \queue -> Just (queue, thing)
  (Parser bigParser) <*> (Parser parser) = Parser result
    where
      result reststream = do
        --TODO find out proper order of the next two lines:
        (reststream, tmpFunction) <- bigParser reststream
        (reststream, tmpOutput) <- parser reststream
        return $ (reststream, tmpFunction tmpOutput)

-- | consecutive apply parser, use the intermediate results to assemble a final result
instance Monad (Parser token) where
  (Parser parser) >>= f = Parser result
    where
      result string = do
        tmp <- parser string
        let tmpResult = output tmp
        let tmpStringRest = leftover tmp
        let newParser = f tmpResult
        runParser newParser tmpStringRest

-- | take first working parser
instance Alternative (Parser token) where
  empty = Parser $ const Nothing
  (Parser parser1) <|> (Parser parser2) = Parser result
    where
      result input
        = listToMaybe
        $ catMaybes
        $ [parser1 input, parser2 input]

instance MonadPlus (Parser token) where

run string (Parser parser) = parser string

