module AbecedarianParser.LexerLike where

import Data.Maybe

import AbecedarianParser.Import
import AbecedarianParser.Core
import AbecedarianParser.Basic

parseComplete parser string = runComplete string parser

eatNewLine = eatExpectedToken '\n'
eatNonNewLineToken = eatExpectedNotToken '\n'

token :: Eq c => c -> Parser c c
token = eatExpectedToken

eatDigit = eatOneOfValidTokens "0123456789"

eatString :: Eq c => [c] -> Parser c [c]
eatString = mapM eatExpectedToken

eatUntilEOL = fmap fst parsing
  where
    parsing = doTwoParser line eatNewLine
    line = greedilyRepeatParser eatNonNewLineToken

eatUntilEOF = greedilyRepeatParser eatOne

eatPOSIXLinesWithRemnants = doTwoParser (greedilyRepeatParser eatUntilEOL) (eatUntilEOF)

consumeUntilString border
  = greedilyRepeatParser
  $ (negativeLookahead $ eatString border) *> eatOne

consumeUntilCanParse parser
  = greedilyRepeatParser
  $ (negativeLookahead $ parser) *> eatOne

firstMatch parser
  = consumeUntilCanParse parser *> parser

matches parser
  = greedilyRepeatParser
  $ firstMatch parser

runComplete string parser
  = fromJust
  $ fmap snd
  $ run string parser'
  where
    parser' = parser <* checkForEOF ()

deintercalateNonEmpty parser separator = do
  init <- greedilyRepeatParser (parser <* separator)
  last <- parser
  return $ init ++ [last]

feast :: [Parser a1 [a2]] -> Parser a1 [a2]
feast = fmap concat . parseAllInSequence
