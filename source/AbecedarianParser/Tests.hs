module AbecedarianParser.Tests where

import AbecedarianParser.Import

import AbecedarianParser.Core
import AbecedarianParser.Basic
import AbecedarianParser.LexerLike

test = (,) "abecedarian tests" $ and [test1, test2, test3, test4, test5, test6, test7, test8, test9, test10]

test1 = (run "+123" (fmap (\x-> x:[x]) eatOne)) == Just ("123", "++")
test2 = (run "^" $ (Parser (\x -> Just (x, (\y -> y:[y])))  ) <*> eatOne) == Just ("","^^")
test3 = (==) 1 $ fromJust $ fromJust $ fromJust
  $ app3
    (Just $ Just $ Just (+1))
    (Just $ Just $ Just 0)
test4 = (==) (Just ("est","tt")) $ run "test" $ (<*>) (Parser (\x -> Just (x, (\x -> x:[x])))) eatOne
test5 = (==) (Just ("st",'e')) $ run "test" $ eatOne >>= (\o -> eatOne)
test6 = (==) (Just ("ftest",'q')) $ run "qftest" $ parseWithAssertOnOutput (== 'q') eatOne
test7 = (==) (Just ("45",('1',('2','3')))) $ run "12345" $ doTwoParser eatOne  $ doTwoParser eatOne eatOne
test8 = (==) "012345678910" $ take 12 $ snd $ fromJust $ run ( flip (++) "#" $ concat $ (map show [0..])) $ greedilyRepeatParser $ eatOneOfValidTokens "0123456789"
test9 = (==) (Just ("",("sdk","123"))) $ run "sdk\n123\n" $ doTwoParser eatUntilEOL eatUntilEOL
test10 = (==) (Just ("",(["1","2","3"],"4"))) $ run "1\n2\n3\n4" $ eatPOSIXLinesWithRemnants

