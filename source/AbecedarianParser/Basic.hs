module AbecedarianParser.Basic where

import AbecedarianParser.Import
import AbecedarianParser.Core

failParser = Parser $ \_ -> Nothing

-- returns () if parsing would be successful
positiveLookahead :: Parser a b -> Parser a ()
positiveLookahead (Parser parser) = Parser result
  where
    result input = do--Maybe
        _ <- parser input
        return (input, ())

-- lets the parser fail if parser would succseed
negativeLookahead :: Parser a b -> Parser a ()
negativeLookahead (Parser parser) = Parser result
  where
    result input = case parser input of
                        Nothing -> return (input, ())
                        _       -> Nothing

checkForEOF outputIfSuccessful = Parser $ parser
  where
    parser [] = Just ([], outputIfSuccessful)
    parser _ = Nothing

eatOne = Parser $ parser
  where
    parser [] = Nothing
    parser (c:cs) = Just (cs, c)

doTwoParser :: (Parser s a) -> (Parser s b) -> (Parser s (a,b))
doTwoParser = liftA2 (,)

--TODO refactoring possible?
parseWithAssertOnOutput :: (b->Bool) -> Parser a b -> Parser a b
parseWithAssertOnOutput assertion (Parser parser) = Parser result
  where
    result input = do--Maybe
        (restStream, value) <- parser input
        _ <- ensure assertion value
        return (restStream, value)

tryParser :: Parser a b -> Parser a (Maybe b)
tryParser (Parser parser) = Parser result
  where
    result input = Just (restStream, fmap snd attempt)
      where
        attempt = parser input
        restStream = fromMaybe input $ fmap leftover attempt

assertToken :: Eq c => c -> Parser s c -> Parser s c
assertToken c = parseWithAssertOnOutput (== c)

assertNotToken :: Eq c => c -> Parser s c -> Parser s c
assertNotToken c = parseWithAssertOnOutput (/= c)

eatExpectedToken c = assertToken c eatOne
eatExpectedNotToken c = assertNotToken c eatOne

firstSuccessfulParser :: [Parser a b] -> Parser a b
firstSuccessfulParser = foldr (<|>) failParser

eatOneOfValidTokens possibleTokens
  = assert check $ firstSuccessfulParser parsers
  where
    check = nub possibleTokens == possibleTokens
    parsers = map eatExpectedToken possibleTokens

parseAllInSequence :: [Parser a b] -> Parser a [b]
parseAllInSequence = sequenceA

--TODO very inefficient
greedilyRepeatParser' :: Parser a b -> Parser a [b]
greedilyRepeatParser' parser = Parser $ result
  where
    result input = parsers
      where
        parsers
          = id
          $ listToMaybe . reverse
          $ takeWhileJust
          $ map (\f -> run input f)
          $ map parseAllInSequence
          $ map (\n -> replicate n parser)
          $ [0..]

--TODO refactor
greedilyRepeatParser :: Parser s o -> Parser s [o]
greedilyRepeatParser parser = Parser $ result
  where
    result input = help $ run input parser
      where
        help Nothing = Just (input, [])
        help (Just (s, o)) = Just $ combine (undefined, o) (recursion s)
    recursion s
      = fromJust
      $ run s (greedilyRepeatParser parser)
    combine (s,o) lazy_recursion = (left, o : right)
      where
        left = fst lazy_recursion
        right = snd lazy_recursion
