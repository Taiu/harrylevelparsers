module AbecedarianParser.Missing where

takeWhileJust :: [Maybe a] -> [a]
takeWhileJust =
   foldr (\x acc -> maybe [] (:acc) x) []

ensure :: (a -> Bool) -> a -> Maybe a
ensure p v
  | p v       = Just v
  | otherwise = Nothing
