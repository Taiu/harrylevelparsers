{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}

, stdenv ? pkgs.stdenv

, ghc ? import ./glasgowHaskellCompiler.nix {}

}:


stdenv.mkDerivation {
  name = "harrylevelparsers";
  builder = ./builder.sh;

  source = ./source;

  buildInputs = [
    ghc
  ];

}
