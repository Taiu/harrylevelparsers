echo "START###############################################"

source $stdenv/setup

temporary=/temporary/

mkdir $temporary
cd $temporary

cp -r $source/* $temporary
runhaskell TestSuite.hs


# dump files
mkdir $out
cp -r $source $out/source
cd $out/source


echo "STOP################################################"
